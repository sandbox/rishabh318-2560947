CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration


INTRODUCTION
------------

Current Maintainer: Rishabh Khare <rishabhkhare88@gmail.com>

A simple integration of braintree gateway.


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

* Download the braintree Library from following URL.
   https://github.com/braintree/braintree_php
   for further information.

* Extract your folder and navigate to braintree_php-master/.
   Now create directory called braintree_php inside sites/all/libraries. Copy
   the contents of braintree_php-master to braintree_php folder that you have
   created in sites/all/libraries.

CONFIGURATION
-------------
Administrator needs to set the braintree configuration. For set up the Braintree
configuration navigate to
<yourdomain.com>/admin/config/braintree_payment_config/configuration

* From here Admin can select the braintree server environment (sandbox/live).
* All the content type listing are showing here. 
* Those content type which are selected here there is one price field will 
create in selected content type.
* Enter the amount in respective node, save it and click on buy button on node
view page.
