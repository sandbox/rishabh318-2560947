<?php

/**
 * @file
 * User for helper functions user in module file.
 */

/**
 * Function used for when payment is cancel.
 */
function simple_braintree_gateway_cancel_payment($order_id) {
  db_delete('braintree_billing_information')
    ->condition('id', $order_id)
    ->execute();

  unset($_SESSION['card_number']);
  unset($_SESSION['owner']);
  unset($_SESSION['exp_month']);
  unset($_SESSION['exp_year']);
  unset($_SESSION['code']);

  drupal_goto('');
  drupal_set_message(t('Your transaction was cancelled.'));
}

/**
 * Function used for when payment is successful.
 */
function simple_braintree_gateway_success() {
  global $user;
  if ($_GET['success_id']) {
    $ori_trans_id = base64_decode(urldecode($_GET['success_id']));
  }
  $result = db_select('braintree_billing_information')
    ->fields('braintree_billing_information')
    ->condition('trans_id', $ori_trans_id)
    ->execute()->fetch();
  // Need to fetch Booking Details from custom table.@TO DO!
  if ($result->uid == $user->uid) {
    return theme('confirmation_page', array(
      'result' => $result,
    ));
  }
  else {
    drupal_access_denied();
  }
}
