<?php

/**
 * @file
 * To display success message.
 */
?>
<div class="success">
  Thank you for making payment <?php print $result->first_name; ?> <?php print $result->last_name; ?>
</div>
