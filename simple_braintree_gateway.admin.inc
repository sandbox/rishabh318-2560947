<?php

/**
 * @file
 * Admin page callback file for the Braintree payment module.
 */

/**
 * Form constructor to the set the api add form.
 *
 * @ingroup forms
 */
function simple_braintree_gateway_payment_config_form($form, &$form_state) {
  $form['simple_braintree_gateway_braintree_merchant_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant ID'),
    '#default_value' => variable_get('simple_braintree_gateway_braintree_merchant_id'),
    '#description' => '<p>' . t('Enter your Merchant ID e.g 87test796vxsvvvr')
    . '</p>',
    '#required' => TRUE,
  );
  $form['simple_braintree_gateway_braintree_public_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Public key'),
    '#default_value' => variable_get('simple_braintree_gateway_braintree_public_key'),
    '#description' => '<p>' . t('Enter your Public key e.g testkr3gpxr9ztcz')
    . '</p>',
    '#required' => TRUE,
  );
  $form['simple_braintree_gateway_braintree_private_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Private key'),
    '#default_value' => variable_get('simple_braintree_gateway_braintree_private_key'),
    '#description' => '<p>' . t('Enter your Public key e.g 12acd4a7ad656781996cc643ab215c67')
    . '</p>',
    '#required' => TRUE,
  );
  $form['simple_braintree_gateway_braintree_environment'] = array(
    '#type' => 'radios',
    '#title' => t('Braintree server'),
    '#options' => array(
      'sandbox' => ('Sandbox - use for testing, requires a Braintree Sandbox account'),
      'production' => ('Production - use for processing real transactions'),
    ),
    '#required' => TRUE,
    '#default_value' => variable_get('simple_braintree_gateway_braintree_environment'),
  );
  $field_name = variable_get('braintree_price_field', '');
  $types = node_type_get_names();
  $types_keys = array_keys($types);
  $default_values = array();
  foreach ($types_keys as $type) {
    if (field_info_instance('node', $field_name, $type)) {
      $default_values[$type] = $type;
    }
    else {
      $default_values[$type] = 0;
    }
  }

  $form['braintree_content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content types'),
    '#options' => $types,
    '#default_value' => $default_values,
    '#description' => t('Check for what content types you want Braintree functionality to have'),
  );
  $form['#submit'][] = 'simple_braintree_gateway_fields_content_types_submit';

  return system_settings_form($form);
}

/**
 * Page callback: Generates a listing of the users paid for.
 *
 * @return string
 *   User listing with payment details.
 */
function simple_braintree_gateway_success_payment_listing() {
  $header = array(
    array('data' => t('ID'), 'field' => 'id', 'sort' => 'asc'),
    array('data' => t('Email'), 'field' => 'email'),
    array('data' => t('Amount (Dollars)'), 'field' => 'amount'),
    array('data' => t('Created'), 'field' => 'created'),
  );
  $rows = array();
  $query = db_select('braintree_billing_information', 'bbi')->fields('bbi');
  $table_sort = $query->extend('TableSort')
    ->orderByHeader($header);
  $pager = $table_sort->extend('PagerDefault')
    ->limit(10);
  $results = $pager->execute();
  foreach ($results as $row) {
    $rows[] = array($row->id,
      check_plain($row->email),
      $row->amount,
      date('d-m-Y : H:i:s', $row->created),
    );
  }
  return theme('table', array(
    'header' => $header,
    'rows' => $rows,
  )) . theme('pager');
}
